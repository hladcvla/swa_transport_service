DROP TABLE transport_type;
DROP TABLE transported_order;

CREATE TABLE IF NOT EXISTS transport_type(id serial PRIMARY KEY, name varchar(255), max_weight integer, cost integer, available_cities varchar(255));
CREATE TABLE IF NOT EXISTS transported_order(id serial PRIMARY KEY, name varchar(255), transport_type_id integer, order_id integer, warehouse_product_id integer, date_of_delivery date, delivered boolean);