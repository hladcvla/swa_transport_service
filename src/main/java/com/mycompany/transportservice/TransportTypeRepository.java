package com.mycompany.transportservice;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.transportservice.model.TransportType;

public interface TransportTypeRepository extends JpaRepository<TransportType, Long> {
}

