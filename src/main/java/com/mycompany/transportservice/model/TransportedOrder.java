package com.mycompany.transportservice.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Vladyslav Hladchenko
 */
@Entity
@Getter
@Setter
@Table(name = "transported_order")
public class TransportedOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "date_of_delivery")
    @NotEmpty
    public Date dateOfDelivery;

    @Column(name = "delivered")
    @NotEmpty
    public Boolean delivered;

    @Column(name = "order_id")
    @NotEmpty
    public Long orderId;

    @Column(name = "transport_type_id")
    @NotEmpty
    public Long transportTypeId;

    @Column(name = "warehouse_product_id")
    @NotEmpty
    public Long warehouseProductId;


}
