package com.mycompany.transportservice.model;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.vladmihalcea.hibernate.type.array.StringArrayType;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Vladyslav Hladchenko
 */

// @TypeDefs({
//     @TypeDef(
//         name = "string-array", 
//         typeClass = StringArrayType.class
//     )
// })
@Entity
@Getter
@Setter
@Table(name = "transport_type")
public class TransportType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "name")
    @NotEmpty
    public String name;

    @Column(name = "max_weight")
    @NotEmpty
    public Integer maxWeight;

    @Column(name = "cost")
    @NotEmpty
    public Integer cost;

    
    @Column(name = "available_cities")
    @NotEmpty
    private String availableCities;
}
