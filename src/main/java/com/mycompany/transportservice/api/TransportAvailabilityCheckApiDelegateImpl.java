package com.mycompany.transportservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;

import com.mycompany.transportservice.TransportTypeRepository;
import com.mycompany.transportservice.model.TransportType;

import com.mycompany.transportservice.api.model.UserDto;

import com.mycompany.transportservice.apiClients.UserServiceApiClient;



@Service

public class TransportAvailabilityCheckApiDelegateImpl implements TransportAvailabilityCheckApiDelegate{
    private final TransportTypeRepository transportTypeRepository;

    private UserServiceApiClient userServiceApiClient;

    public TransportAvailabilityCheckApiDelegateImpl(TransportTypeRepository transportTypeRepository,
                                                    UserServiceApiClient userServiceApiClient
    ) {
        this.transportTypeRepository = transportTypeRepository;
        this.userServiceApiClient = userServiceApiClient;

    }


    public ResponseEntity<String> transportAvailabilityCheck(Long transportTypeId,
                                                           Long warehouseProductId,
                                                           String deliveryAddress) {

        System.out.println("transportAvailabilityCheck:\ntransportTypeId " + transportTypeId + "\nwarehouseProductId " + warehouseProductId + "\ndeliveryAddress " + deliveryAddress);
        
        Optional<TransportType> tto = transportTypeRepository.findById(transportTypeId);

        // ResponseEntity<UserDto> userResponse = userServiceApiClient.getUserById(userId);

        if(!tto.isPresent())
        {
            return ResponseEntity.notFound().build();
        }

        // UserDto userDto = userResponse.getBody();
        TransportType tt = tto.get();

        List<String> availableCities = new ArrayList<>(Arrays.asList(tt.getAvailableCities().split(";")));


        System.out.println("Check availability for transport " + tt.getName() + ", available to:"  + availableCities.stream().collect(Collectors.joining(",")) + ", to user address: '" +deliveryAddress + "'");

        // if(! Arrays.asList(tt.getAvailableCities()).contains(deliveryAddress))
        // {
        //     System.out.println("No availability for transport " + tt.getName() + " to user address: " +deliveryAddress);

        //     return ResponseEntity.notFound().build();
        // }

        System.out.println("substring index : " + tt.getAvailableCities().indexOf(deliveryAddress));
        if(tt.getAvailableCities().indexOf(deliveryAddress) == -1)
        {
            System.out.println("No availability for transport " + tt.getName() + " to user address: " +deliveryAddress);

            return ResponseEntity.notFound().build();
        }

        System.out.println("transport available !");


        return ResponseEntity.ok("2020-10-10"); //TODO
    }

}
