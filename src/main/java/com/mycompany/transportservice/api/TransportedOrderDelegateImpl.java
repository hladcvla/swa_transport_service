package com.mycompany.transportservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;

import com.mycompany.transportservice.TransportedOrderRepository;
import com.mycompany.transportservice.api.model.TransportedOrderDto;
import com.mycompany.transportservice.mappers.TransportedOrderMapper;
import com.mycompany.transportservice.model.TransportedOrder;

@Service
public class TransportedOrderDelegateImpl implements TransportedOrderApiDelegate{
    private final TransportedOrderRepository transportedOrderRepository;


    public TransportedOrderDelegateImpl(TransportedOrderRepository transportedOrderRepository) {
        this.transportedOrderRepository = transportedOrderRepository;
    }

    public ResponseEntity<TransportedOrderDto> getTransportedOrderById(Long transportedOrderId) {
        System.out.println("getTransportedOrderById() :  " + transportedOrderId);

        Optional<TransportedOrder> transportOptional = transportedOrderRepository.findById(transportedOrderId);

        if( transportOptional.isPresent())
        {
            System.out.println("Transport type is found in db : ");

            System.out.println("db object id : " + transportOptional.get());
            
            TransportedOrderDto dto = TransportedOrderMapper.INSTANCE.mapTo(transportOptional.get());

            System.out.println("mapped dto : " + dto);

            return ResponseEntity.ok(dto);
        }

        return ResponseEntity.notFound().build();


        // return Optional
        //     .ofNullable( transportOptional )
        //     .map( transport -> ResponseEntity.ok().body(TransportMapper.INSTANCE.mapTo(transport.get())) )          //200 OK
        //     .orElseGet( () -> ResponseEntity.notFound().build() );  //404 Not found
        

    }

    @Override
    public ResponseEntity<TransportedOrderDto> addTransportedOrder(TransportedOrderDto transportedOrderDto) {

        System.out.println("addTransportedOrder transportedOrderDto :  " + transportedOrderDto);

        TransportedOrder new_entity = TransportedOrderMapper.INSTANCE.mapTo(transportedOrderDto);

        System.out.println("addTransportedOrder TransportedOrder  :  " + new_entity);

        TransportedOrder saved_entity = transportedOrderRepository.save(new_entity);

        TransportedOrderDto saved_dto = TransportedOrderMapper.INSTANCE.mapTo(saved_entity);

        System.out.println("addTransportedOrder after add dto :  " + saved_dto);

        return ResponseEntity.ok(saved_dto);
    }

    public ResponseEntity<Void> deleteTransportedOrder(Long transportedOrderId, String apiKey) {

        transportedOrderRepository.deleteById(transportedOrderId);

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<TransportedOrderDto> updateTransportedOrder(TransportedOrderDto transportedOrderDto) {

        System.out.println("updateTransportedOrder transportedOrderDto :  " + transportedOrderDto);

        TransportedOrder entity_to_update = TransportedOrderMapper.INSTANCE.mapTo(transportedOrderDto);
        TransportedOrder updated_entity = transportedOrderRepository.save(entity_to_update);

        TransportedOrderDto saved_dto = TransportedOrderMapper.INSTANCE.mapTo(updated_entity);
        
        return ResponseEntity.ok(saved_dto);
    }
}
