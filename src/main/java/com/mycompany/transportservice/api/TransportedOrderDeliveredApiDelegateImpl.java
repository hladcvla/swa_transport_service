package com.mycompany.transportservice.api;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;

import com.mycompany.transportservice.TransportedOrderRepository;
import com.mycompany.transportservice.api.model.TransportedOrderDto;
import com.mycompany.transportservice.apiClients.WarehouseProductServiceApiClient;
import com.mycompany.transportservice.mappers.TransportedOrderMapper;
import com.mycompany.transportservice.model.TransportedOrder;

@Service
public class TransportedOrderDeliveredApiDelegateImpl implements TransportedOrderDeliveredApiDelegate {

    private final TransportedOrderRepository transportedOrderRepository;
    private final WarehouseProductServiceApiClient warehouseServiceApiClient;

    public TransportedOrderDeliveredApiDelegateImpl(TransportedOrderRepository transportedOrderRepository, WarehouseProductServiceApiClient warehouseServiceApiClient) {
        this.transportedOrderRepository = transportedOrderRepository;
        this.warehouseServiceApiClient = warehouseServiceApiClient;
    }

    @Override
    public ResponseEntity<Void> transportedOrderDelivered(Long transportedOrderId) {
        System.out.println("transportOrderDelivered() :  " + transportedOrderId);

        Optional<TransportedOrder> transportedOrderOptional = transportedOrderRepository.findById(transportedOrderId);

        if(!transportedOrderOptional.isPresent())
        {
            return ResponseEntity.notFound().build();
        }

        ResponseEntity<Void> warehouse_response = warehouseServiceApiClient.deleteWarehouseProduct(transportedOrderOptional.get().getWarehouseProductId(), "");

        System.out.println("transportOrderDelivered warehouse response: " + warehouse_response.toString());

        if(warehouse_response.getStatusCode() != HttpStatus.OK)
        {
            return ResponseEntity.notFound().build();
        }


        return ResponseEntity.ok().build();
    }

    
}
