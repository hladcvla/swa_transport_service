package com.mycompany.transportservice.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;

import com.mycompany.transportservice.TransportTypeRepository;
import com.mycompany.transportservice.api.model.TransportTypeDto;
import com.mycompany.transportservice.mappers.TransportTypeMapper;
import com.mycompany.transportservice.model.TransportType;

@Service
public class TransportTypeDelegateImpl implements TransportTypeApiDelegate {
    private final TransportTypeRepository transportTypeRepository;

    public TransportTypeDelegateImpl(TransportTypeRepository transportTypeRepository) {
        this.transportTypeRepository = transportTypeRepository;
    }

    @Override
    public ResponseEntity<TransportTypeDto> getTransportTypeById(Long transportTypeId) {
        System.out.println("getTransportTypeById() :  " + transportTypeId);

        Optional<TransportType> transportOptional = transportTypeRepository.findById(transportTypeId);

        if( transportOptional.isPresent())
        {
            System.out.println("Transport type is found in db : ");

            System.out.println("db object id : " + transportOptional.get());
            
            TransportTypeDto dto = TransportTypeMapper.INSTANCE.mapTo(transportOptional.get());

            System.out.println("mapped dto : " + dto);  

            return ResponseEntity.ok(dto);
        }

        return ResponseEntity.notFound().build();
      

    }

    @Override
    public ResponseEntity<TransportTypeDto> addTransportType(TransportTypeDto transportTypeDto) {

        if(transportTypeDto.getAvailableCities().isEmpty() || transportTypeDto.getName().isEmpty() || transportTypeDto.getCost() <0 || transportTypeDto.getMaxWeight() < 0)
        {
            System.out.println("addTransportType: bad transportTypeDto fields.");
            return ResponseEntity.badRequest().build();
        }

        System.out.println("addTransportType transportTypeDto :  " + transportTypeDto);

        TransportType new_entity = TransportTypeMapper.INSTANCE.mapTo(transportTypeDto);

        System.out.println("addTransportType TransportType  :  " + new_entity);

        TransportType saved_entity = transportTypeRepository.save(new_entity);

        TransportTypeDto saved_dto = TransportTypeMapper.INSTANCE.mapTo(saved_entity);

        System.out.println("addTransportType after add dto :  " + saved_dto);

        return ResponseEntity.ok(saved_dto);
    }

    @Override
    public ResponseEntity<Void> deleteTransportType(Long transportTypeId, String apiKey) {

        transportTypeRepository.deleteById(transportTypeId);

        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<TransportTypeDto> updateTransportType(TransportTypeDto transportTypeDto) {

        System.out.println("updateTransportType transportTypeDto :  " + transportTypeDto);

        TransportType entity_to_update = TransportTypeMapper.INSTANCE.mapTo(transportTypeDto);
        TransportType updated_entity = transportTypeRepository.save(entity_to_update);

        TransportTypeDto saved_dto = TransportTypeMapper.INSTANCE.mapTo(updated_entity);
        
        return ResponseEntity.ok(saved_dto);
    }
}
