package com.mycompany.transportservice.apiClients;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import io.swagger.v3.oas.annotations.Parameter;
import com.mycompany.transportservice.api.model.UserDto;

@FeignClient(
    name = "user-service",
    fallback = UserServiceApiClient.UserServiceApiClientFallback.class)
public interface UserServiceApiClient {

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/user/{userId}",
        produces = { "application/json" }
    )
    public ResponseEntity<UserDto> getUserById(
        @Parameter(name = "userId", required = true) @PathVariable("userId") Long userId
    );


    @Component
    class UserServiceApiClientFallback {


        @RequestMapping(
            method = RequestMethod.GET,
            value = "/user/{userId}",
            produces = { "application/json" }
        )
        public ResponseEntity<UserDto> getUserById(
            @Parameter(name = "userId", required = true) @PathVariable("userId") Long userId
        ) {
            return ResponseEntity.notFound().build();
        }

    }
}
