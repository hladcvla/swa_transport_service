package com.mycompany.transportservice.apiClients;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import io.swagger.v3.oas.annotations.Parameter;

import com.mycompany.transportservice.api.model.WarehouseProductDto;


@FeignClient(
    name = "warehouse-service",
    fallback = WarehouseProductServiceApiClient.WarehouseProductApiClientFallback.class)
public interface WarehouseProductServiceApiClient {

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = "/warehouseProduct/{warehouseProductId}"
    )
    public ResponseEntity<Void> deleteWarehouseProduct(
        @Parameter(name = "warehouseProductId", required = true) @PathVariable("warehouseProductId") Long warehouseProductId,
        @Parameter(name = "api_key") @RequestHeader(value = "api_key", required = false) String apiKey
    );


    @Component
    class WarehouseProductApiClientFallback {
        @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/warehouseProduct/{warehouseProductId}"
        )
        public  ResponseEntity<Void> deleteWarehouseProduct(    
            @Parameter(name = "warehouseProductId", required = true) @PathVariable("warehouseProductId") Long warehouseProductId,
            @Parameter(name = "api_key") @RequestHeader(value = "api_key", required = false) String apiKey
        ){
            System.out.println("WarehouseProductApiClientFallback deleteWarehouseProduct");
            return ResponseEntity.notFound().build();
        }

    }
}
