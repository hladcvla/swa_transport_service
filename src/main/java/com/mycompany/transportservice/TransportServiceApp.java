package com.mycompany.transportservice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.ResponseEntity;

import com.mycompany.transportservice.api.model.WarehouseProductDto;
import com.mycompany.transportservice.apiClients.WarehouseProductServiceApiClient;
import com.mycompany.transportservice.apiClients.UserServiceApiClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class TransportServiceApp {

    Logger logger = LoggerFactory.getLogger(TransportServiceApp.class);

    @Autowired
    private WarehouseProductServiceApiClient warehouseProductApiClient;

    @Autowired
    private UserServiceApiClient userServiceApiClient;

    @Autowired
    private DiscoveryClient discoveryClient;


    public static void main(String[] args) {
        SpringApplication.run(TransportServiceApp.class, args);
    }

    @PostConstruct
    public void start() {
        logger.info("Starting ...");
        // get the information about the service programmatically
        List<ServiceInstance> instances = this.discoveryClient.getInstances("warehouse-service");
        List<String> uris = instances.stream().map(i -> i.getUri().toString()).collect(Collectors.toList());
        logger.info("warehouse-service running at {}", String.join(", ", uris));
        
        // ResponseEntity<WarehouseProductDto> resp = warehouseProductApiClient.getWarehouseProductById(1L);

        // System.out.println("WH resp:  " + resp.toString());
	}

}

@RestController
class ServiceInstanceRestController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("/service-instances/{applicationName}")
    public List<ServiceInstance> serviceInstancesByApplicationName(
            @PathVariable String applicationName) {
        return this.discoveryClient.getInstances(applicationName);
    }
}
