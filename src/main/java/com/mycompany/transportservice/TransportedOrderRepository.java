package com.mycompany.transportservice;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mycompany.transportservice.model.TransportedOrder;

public interface TransportedOrderRepository extends JpaRepository<TransportedOrder, Long> {
}

