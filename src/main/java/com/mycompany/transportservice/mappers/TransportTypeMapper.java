package com.mycompany.transportservice.mappers;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.mycompany.transportservice.api.model.TransportTypeDto;
import com.mycompany.transportservice.model.TransportType;

@Mapper
public interface TransportTypeMapper {
    public static TransportTypeMapper INSTANCE = Mappers.getMapper(TransportTypeMapper.class);

    TransportTypeDto mapTo(TransportType t);
    TransportType mapTo(TransportTypeDto tDto);
}