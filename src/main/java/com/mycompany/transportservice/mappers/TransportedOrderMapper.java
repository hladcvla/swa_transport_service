package com.mycompany.transportservice.mappers;


import java.sql.Date;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.mycompany.transportservice.api.model.TransportedOrderDto;
import com.mycompany.transportservice.model.TransportedOrder;

@Mapper
public interface TransportedOrderMapper {
    public static TransportedOrderMapper INSTANCE = Mappers.getMapper(TransportedOrderMapper.class);

    TransportedOrderDto mapTo(TransportedOrder t);

    TransportedOrder mapTo(TransportedOrderDto tDto);
    
    default Date map(String dateString)
    {
        return java.sql.Date.valueOf(dateString);
    }
}