package com.mycompany.transportservice.api;

import com.mycompany.transportservice.model.TransportType;
import com.mycompany.transportservice.TransportServiceApp;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.transportservice.mappers.TransportTypeMapper;
import com.mycompany.transportservice.api.model.TransportTypeDto;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TransportServiceApp.class, properties = {"spring.cloud.discovery.client.simple.instances.user-service[0].uri=http://localhost:9561",
                                                                    "spring.cloud.discovery.client.simple.instances.warehouse-service[0].uri=http://localhost:9562"})
class TransportTypeDelegateImplTest {

    @Autowired
    TransportTypeDelegateImpl transportTypeDelegateImpl;

    public TransportType initialize_transport_type(   String transportName,
                                            String availableCities,
                                            Integer cost,
                                            Integer maxWeight) {
        TransportType testedTransportType = new TransportType();

        if(transportName.equals("empty"))
        {
            transportName="";
        }
        if(availableCities.equals("empty"))
        {
            availableCities="";
        }

        testedTransportType.setAvailableCities(availableCities);
        testedTransportType.setName(transportName);
        testedTransportType.setCost(cost);
        testedTransportType.setMaxWeight(maxWeight);

        return testedTransportType;
    }

    @ParameterizedTest
    @CsvSource({"Post,A;B;C,10,10,200",
                "Post,empty,10,10,400",
                "empty,A;B;C,10,10,400",
                "Post,A;B;C,-10,10,400",
                "Post,A;B;C,10,-10,400"})
    void addTransportTypeTest(String transportName,
                              String availableCities,
                              Integer cost,
                              Integer maxWeight,
                              String expectedResponse) {

        TransportType testestedTransportType = initialize_transport_type(transportName,availableCities,cost,maxWeight);

        TransportTypeDto testTransportTypeDto = TransportTypeMapper.INSTANCE.mapTo(testestedTransportType);

        ResponseEntity<TransportTypeDto> response = transportTypeDelegateImpl.addTransportType(testTransportTypeDto);
        assertEquals(Integer.parseInt(expectedResponse), response.getStatusCode().value());
    }

    @ParameterizedTest
    @CsvSource({"1,200",
                "2,404"})
    void getTransportTypeByIdTest(Long id, String expectedResponse) {


        ResponseEntity<TransportTypeDto> response = transportTypeDelegateImpl.getTransportTypeById(id);

        assertEquals(Integer.parseInt(expectedResponse), response.getStatusCode().value());

        if(expectedResponse.equals("200"))
        {
            assertEquals(id, response.getBody().getId());
        }
        else
        {
            assertNull(response.getBody());
        }

    }
}
