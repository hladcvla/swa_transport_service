DROP TABLE IF EXISTS transport_type;
DROP TABLE IF EXISTS transported_order;

CREATE TABLE transport_type(id serial PRIMARY KEY, name varchar(255), max_weight integer, cost integer, available_cities varchar(255));
CREATE TABLE transported_order(id serial PRIMARY KEY, name varchar(255), transport_type_id integer, order_id integer, warehouse_product_id integer, date_of_delivery date, delivered boolean);

INSERT INTO transport_type(id, name, max_weight, cost, available_cities) VALUES(1, 'Post - delivery to hand', 30, 100, 'Prague,Ostrava' );
