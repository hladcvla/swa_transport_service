FROM maven:latest
COPY settings.xml /usr/share/maven/conf/settings.xml
ADD . /projects/transport-service
WORKDIR /projects/transport-service
CMD ["mvn", "spring-boot:run"]
